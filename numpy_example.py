import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


def sinusoidal(x, freq, phase, mean, amplitude):
    return mean + amplitude * np.sin(2 * np.pi * freq * x + phase)


def create_oscillating_lightcurve(length = 20, bin_time=0.01, mean = 1000,
                                  amplitude = 400, freq = 1., phase=0):
    '''Create a sinusoidal light curve with Poissonian variability.

    Inputs: 
        length: duration of the observation
        M:      mean value of the light curve
        A:      amplitude of the oscillation
        freq:   frequency of the oscillation
    Returns:
        time:   array of light curve times
        lc:     the light curve array'''

    time = np.arange(0, length, bin_time)

    continuous_lc = sinusoidal(time, freq, phase, mean=mean,
                               amplitude=amplitude)

    # Numpy.random.poisson accepts either a mean value and a number of draws,
    # or a light curve that will be used as the local mean value
    lc = np.random.poisson(continuous_lc)

    return time, lc


def power_spectrum(time, lc):
    '''Calculates the power spectrum.

    Inputs:
        time:   time array
        lc:     light curve array
    Return values:
        freqs:  Frequencies
        pds:    Power spectrum
    '''
    # Calculate the FFT
    ft = np.fft.fft(lc)
    # and the PDS
    pds = (ft * ft.conjugate()).real

    # Calculate the FFT frequencies using NumPy's utility.
    sampling = time[1] - time[0]
    freqs = np.fft.fftfreq(len(lc), sampling)
    # Find max power (discarding frequencies <=0!) and return corresponding
    # frequency
    filter = freqs > 0

    return freqs[filter], pds[filter]


def find_best_frequency(time, lc):
    '''Find the frequency of the oscillation through the power spectrum.
        
    Does this ring a bell, pulsar people :)?
        
    Inputs:
        time:   time array
        lc:     light curve array
    Return value:
        f0:     the frequency of the maximum power'''

    freqs, power = power_spectrum(time, lc)

    plt.figure("PDS")
    plt.plot(freqs, power, drawstyle="steps-mid")
    plt.xlabel("Frequency")
    plt.ylabel("Power")

    maxpow = np.argmax(power)
    return freqs[maxpow]


# In principle, we don't know the frequency and the other parameters of the
# light curve. Let's use all the defaults but the frequency and phase of 
# the pulsation
time, lc = create_oscillating_lightcurve(freq=2.233, phase=0.3123)

# Poissonian: the error on the light curve is the square root
elc = np.sqrt(lc)

# Let's try to fit the light curve with the sinusoidal function
# Initial parameters:
f0 = find_best_frequency(time, lc) # estimate of the frequency
ph0 = 0  # initial phase
mean0 = np.mean(lc)  # estimate of the mean
ampl0 = (np.max(lc) - np.min(lc)) / 2.  # estimate of the amplitude

pars, pcov = curve_fit(sinusoidal, time, lc, [f0, ph0, mean0, ampl0],
                       sigma=elc,
                       absolute_sigma=True)  # Sigma is real stdev, not weights

print("Parameters:", pars)
#------ PLOT with two subplots ------

# This create two identical subplots, sharing the X axis
f, axarr = plt.subplots(2, sharex=True)
# Here I'm setting to zero the space between the two
f.subplots_adjust(hspace=0)

# -- Upper panel
axarr[0].errorbar(time, lc, yerr=elc, ecolor='k', fmt="none")
# Notice the way to say "From now on, these parameters are in the right order
# for being given as function arguments"
axarr[0].plot(time, sinusoidal(time, *pars), lw=2, color='r')
axarr[0].set_ylabel("LC")

# -- Lower panel
axarr[1].errorbar(time,
                  lc - sinusoidal(time, *pars),
                  yerr=elc, ecolor='k', fmt="none")
axarr[1].axhline(1, lw=2, color='r')
axarr[1].set_ylabel("Residual")
axarr[1].set_xlabel("Time")

# Show it!
plt.show()
